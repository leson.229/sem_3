﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCodeFirstApplications
{
    public class FptSchoolContext : DbContext
    {
        // base() -> MAC DINH KET NOI localDb
        // base ("tencuadb") -> db local sql Express co san
        // base ("name = ten cua connection string")
        public FptSchoolContext() : base("FptDbCourseFirst")
        {
            Database.SetInitializer<FptSchoolContext>(new FptSchoolDbInitializer());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new StudentConfiguarions());
            modelBuilder.Entity<Teacher>()
                .ToTable("teacherTable");
            modelBuilder.Entity<Teacher>()
            .MapToStoredProcedures();
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Student> students { get; set; }
        public DbSet<Grade> grades { get; set; }
        public DbSet<Course> courses{get;set;}
        public DbSet<StudentAddress> studentAddress { get; set; }
    }
}
